/**
 * Created by Виктория on 07.06.2016.
 */
import java.io.IOException;
import java.util.List;


public class UI {
    static FileHelper fh = new FileHelper();
    private static List<postfixToken> getPostfixTokens;

    public static void main (String[] args) throws Exception {
                validInput();
    }


    static void process (String file) throws Exception {
        Lexer lexer =  new Lexer();
        lexer.processFile(file);
        List<Token> tokens = lexer.setTokens();
        Parser parser = new Parser();
        parser.setTokens(tokens);
        try {
            parser.lang();
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        List<postfixToken> postfixTokens = parser.getPostfixTokens();
        PolizProcessor processor = new PolizProcessor(postfixTokens);
        processor.calculate();
    }

    static void validInput() throws Exception {
        fh.fileInput("C:\\Users\\Виктория\\Downloads\\SPO_updated1\\kursach\\src\\valid-test.input");
        process("C:\\Users\\Виктория\\Downloads\\SPO_updated1\\kursach\\src\\valid-test.input");
    }

}

