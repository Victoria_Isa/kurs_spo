/**
 * Created by Виктория on 07.06.2016.
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lexer {

    List<Token> tokens = new ArrayList<Token>();

    String akk = "";

    private Pattern semicolon = Pattern.compile("^;$");
    private Pattern var_keyword = Pattern.compile("^var$");
    private Pattern do_keyword = Pattern.compile("^do$");
    private Pattern while_keyword = Pattern.compile("^while$");
    private Pattern open = Pattern.compile("^\\($");
    private Pattern close = Pattern.compile("^\\)$");
    private Pattern open_loop = Pattern.compile("^\\{$");
    private Pattern close_loop = Pattern.compile("^\\}$");
    private Pattern digit = Pattern.compile("^0|[1-9]{1}[0-9]*$");
    private Pattern var = Pattern.compile("^[a-zA-Z]+$");
    private Pattern addop = Pattern.compile("^\\+$");
    private Pattern multiplyop = Pattern.compile("^\\*$");
    private Pattern divideop = Pattern.compile("^\\/$");
    private Pattern subop = Pattern.compile("^\\-$");
    private Pattern space = Pattern.compile("^\\s*$");
    private Pattern assign_op = Pattern.compile("^=$");
    private Pattern compare_op = Pattern.compile("^>|<|==|>=|<=$");

    private Map<String, Pattern> keyWords = new HashMap<String, Pattern>();
    private Map<String, Pattern> otherTerminals = new HashMap<String, Pattern>();

    private int i;

    private String currentRight = null;

    public Lexer() {

        keyWords.put("VAR_KW", var_keyword);
        keyWords.put("DO_KW", do_keyword);
        keyWords.put("WHILE_KW", while_keyword);


        otherTerminals.put("WS", space);
        otherTerminals.put("SC", semicolon);
        otherTerminals.put("VAR", var);
        otherTerminals.put("DIGIT", digit);
        otherTerminals.put("OPEN", open);
        otherTerminals.put("CLOSE", close);
        otherTerminals.put("LOOP_OPEN", open_loop);
        otherTerminals.put("LOOP_CLOSE", close_loop);
        otherTerminals.put("INCOP", addop);
        otherTerminals.put("DECOP", subop);
        otherTerminals.put("MULTOP", multiplyop);
        otherTerminals.put("DIVOP", divideop);
        otherTerminals.put("ASSIGN_OP", assign_op);
        otherTerminals.put("COMPOP", compare_op);

    }

    public void processFile(String filename) throws IOException {
        File file = new File(filename);
        Reader reader = new FileReader(file);
        BufferedReader breader = new BufferedReader(reader);
        String line;

        System.out.println("START LEXER:\n");
        while((line = breader.readLine()) != null) {
            processString(line);
        }

        if (currentRight == null) {
            throw new IOException("Expression: " + akk + " doesn't matches with any token\n");
        }

        System.out.println("TOKEN + "+ currentRight +" recognized with value "+ akk);


        tokens.add(new Token(currentRight, akk));
        System.out.println("\nLIST OF TOKENS\n***************");
        for (Token token: tokens) {
            System.out.println(token);
        }
    }

    public void processString(String line) {
        for (i=0; i<line.length(); i++) {
            akk = akk + line.charAt(i);
            processAccum();
        }

    }


    private void processAccum() {
        boolean found = false;

        for (String keyWordName : keyWords.keySet()) {
            Pattern newPattern = keyWords.get(keyWordName);
            Matcher match = newPattern.matcher(akk);
            if (match.matches()) {
                currentRight = keyWordName;
                found = true;
            }
        }

        if (currentRight != null && found) {
            System.out.println("TOKEN "+ currentRight +" recognized with value: "+ akk.substring(0, akk.length()));
            tokens.add(new Token (currentRight, akk.substring(0, akk.length())));
            akk = "";
            currentRight = null;
        }

        for (String regExpName : otherTerminals.keySet()) {
            Pattern currentPattern = otherTerminals.get(regExpName);
            Matcher m = currentPattern.matcher(akk);
            if (m.matches()) {
                currentRight = regExpName;
                found = true;
            }

        }
        if (currentRight != null && !found) {
            System.out.println("TOKEN "+ currentRight +" recognized with value: "+ akk.substring(0, akk.length()-1));
            tokens.add(new Token (currentRight, akk.substring(0, akk.length()-1)));
            i--;
            akk = "";
            currentRight = null;
        }
    }

    public List<Token> setTokens() {
        return tokens;
    }


}

